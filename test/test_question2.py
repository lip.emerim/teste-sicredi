"""Contain question 2 tests."""

import unittest
from unittest.mock import patch, Mock

from assertpy import assert_that

from src.question2 import Bin, DescendingFirstFit, Orders


class TestBin(unittest.TestCase):
    """Implement Bin class test cases."""

    def setUp(self) -> None:
        """Add a Bin instance to self before each test."""
        self.bin = Bin(100, 2)

    def test_init(self) -> None:
        """
        Check that the init method if populating fields correctly.

        :return:
        """
        assert_that(self.bin.current_weight).is_equal_to(0)
        assert_that(self.bin.item_count).is_equal_to(0)
        assert_that(self.bin.items).is_empty()
        assert_that(self.bin.weight_capacity).is_equal_to(100)
        assert_that(self.bin.item_capacity).is_equal_to(2)

    def test_fits_true(self) -> None:
        """
        Test that the fits method is returning True when it should.

        :return:
        """
        assert_that(self.bin.fits(weight=60)).is_true()

    def test_fits_excess_weight(self) -> None:
        """
        Test that the fits method is returning False when the weight limit is exceeded.

        :return:
        """
        self.bin.add_item(item='item 1', weight=70)

        assert_that(self.bin.fits(weight=31)).is_false()

    def test_fits_excess_item_count(self) -> None:
        """
        Test that the fits method is returning False when the item limit is exceeded.

        :return:
        """
        self.bin.add_item(item='item 1', weight=10)
        self.bin.add_item(item='item 2', weight=10)

        assert_that(self.bin.fits(weight=31)).is_false()

    def test_get_free_weight(self) -> None:
        """
        Test that the free weight calculation is working properly.

        :return:
        """
        assert_that(self.bin.get_free_weight()).is_equal_to(100)

        self.bin.add_item(item='item 1', weight=70)

        assert_that(self.bin.get_free_weight()).is_equal_to(30)

    def test_full_no_free_weight(self) -> None:
        """
        Test that the full method is returning False when the bin's weight capacity is full.

        :return:
        """
        self.bin.add_item(item='item 1', weight=100)

        assert_that(self.bin.full()).is_true()

    def test_full_no_item_slot(self) -> None:
        """
        Test that the full method is returning False when the bin's item capacity is full.

        :return:
        """
        self.bin.add_item(item='item 1', weight=10)
        self.bin.add_item(item='item 2', weight=10)

        assert_that(self.bin.full()).is_true()

    def test_full_false(self) -> None:
        """
        Test that the full method is returning False when the bin is not full.

        :return:
        """
        assert_that(self.bin.full()).is_false()

        self.bin.add_item(item='item 1', weight=40)

        assert_that(self.bin.full()).is_false()

    def test_add(self) -> None:
        """
        Test the add method is working, without validation.

        :return:
        """
        self.bin.add_item(item='item 1', weight=40, validate=False)

        assert_that(self.bin.items).is_equal_to(['item 1'])
        assert_that(self.bin.current_weight).is_equal_to(40)
        assert_that(self.bin.item_count).is_equal_to(1)
        assert_that(self.bin.get_free_weight()).is_equal_to(60)

    def test_add_validate_valid(self) -> None:
        """
        Test that the add_item method is working, with validation but the item is valid.

        :return:
        """
        self.bin.add_item(item='item 1', weight=40)

        assert_that(self.bin.items).is_equal_to(['item 1'])
        assert_that(self.bin.current_weight).is_equal_to(40)
        assert_that(self.bin.item_count).is_equal_to(1)
        assert_that(self.bin.get_free_weight()).is_equal_to(60)

    def test_add_validate_invalid(self) -> None:
        """
        Test that the add_item method is working, with validation and an invalid item.

        :return:
        """
        assert_that(self.bin.add_item).raises(ValueError).when_called_with('item 1', 101)


class TestDescendingFirstFit(unittest.TestCase):
    """Test cases for the DescendingFirstFitClass."""

    dataset1 = [70, 30, 10, 50, 40, 80, 20, 45, 35, 23]
    dataset2 = [18, 12, 15, 49, 35, 23, 44, 98, 65, 65, 43, 21, 39]

    def setUp(self) -> None:
        """Add a DescendingFirstFit instance to self before each test."""
        self.descending_first_fit = DescendingFirstFit(bin_item_capacity=2, bin_weight_capacity=100)

    def test_init(self) -> None:
        """
        Test the init method is working correctly.

        :return:
        """
        assert_that(self.descending_first_fit.most_free_weight_bin).is_none()
        assert_that(self.descending_first_fit.available_bins).is_empty()
        assert_that(self.descending_first_fit.bin_item_capacity).is_equal_to(2)
        assert_that(self.descending_first_fit.bin_weight_capacity).is_equal_to(100)
        assert_that(self.descending_first_fit.bin_count).is_equal_to(0)

    def test_init_invalid_bin_weight(self) -> None:
        """
        Test that the init method raises an Error when an invalid bin weight is supplied.

        :return:
        """
        with self.assertRaises(ValueError):
            DescendingFirstFit(bin_weight_capacity=0, bin_item_capacity=2)

    def test_init_invalid_bin_item_count(self) -> None:
        """
        Test that the init method raises an Error when an invalid item count is supplied.

        :return:
        """
        with self.assertRaises(ValueError):
            DescendingFirstFit(bin_weight_capacity=100, bin_item_capacity=0)

    def test_initialize(self) -> None:
        """
        Test that the initialize function is correctly resetting the object's state.

        :return:
        """
        self.descending_first_fit.most_free_weight_bin = 5
        self.descending_first_fit.bin_count = 14
        self.descending_first_fit.available_bins = 19

        self.descending_first_fit.initialize()

        assert_that(self.descending_first_fit.most_free_weight_bin).is_none()
        assert_that(self.descending_first_fit.available_bins).is_empty()
        assert_that(self.descending_first_fit.bin_count).is_equal_to(0)

    def test_put_item_into_available_bin(self) -> None:
        """
        Test the put_item_into_available_bin is working correctly.

        :return:
        """
        new_bin = Bin(weight_capacity=100, item_capacity=2)

        self.descending_first_fit.available_bins.append(new_bin)

        assert_that(self.descending_first_fit.put_item_into_available_bin(item=40)).is_true()

        assert_that(self.descending_first_fit.available_bins).is_equal_to([new_bin])

    def test_put_item_into_available_bin_fill_bin(self) -> None:
        """
        Test the put_item_into_available_bin is working correctly when the available bin is made full by the new item.

        :return:
        """
        self.descending_first_fit.put_item_into_new_bin(item=10)

        assert_that(self.descending_first_fit.put_item_into_available_bin(item=30)).is_true()

        # The bin is full, so it should have been removed
        assert_that(self.descending_first_fit.available_bins).is_empty()

    def test_put_item_into_available_bin_no_bins_available(self) -> None:
        """
        Test the put_item_into_available_bin returns False when there are no bins available.

        :return:
        """
        assert_that(self.descending_first_fit.put_item_into_available_bin(item=10)).is_false()

    def test_put_item_into_available_bin_does_not_fit_any_bin(self) -> None:
        """
        Test the put_item_into_available_bin returns False when item does not fit.

        :return:
        """
        new_bin = Bin(weight_capacity=100, item_capacity=2)

        self.descending_first_fit.available_bins.append(new_bin)

        assert_that(self.descending_first_fit.put_item_into_available_bin(item=60)).is_true()
        assert_that(self.descending_first_fit.put_item_into_available_bin(item=50)).is_false()

    def test_insert_item_into_new_bin_new_bin_most_free_weight(self) -> None:
        """
        Test the insert_item_into_new_bin method is working when the new bin is the one with most free weight.

        :return:
        """
        self.descending_first_fit.put_item_into_new_bin(item=80)

        assert_that(self.descending_first_fit.available_bins).is_length(1)
        assert_that(self.descending_first_fit.most_free_weight_bin.get_free_weight()).is_equal_to(20)

        self.descending_first_fit.put_item_into_new_bin(item=10)

        assert_that(self.descending_first_fit.available_bins).is_length(2)
        # Since the new bin has max weight, it should replace the old one
        assert_that(self.descending_first_fit.most_free_weight_bin.get_free_weight()).is_equal_to(90)

    def test_insert_item_into_new_bin_new_bin_not_most_free_weight(self) -> None:
        """
        Test the insert_item_into_new_bin method is working when the new bin is not the one with most free weight.

        :return:
        """
        self.descending_first_fit.put_item_into_new_bin(item=20)

        assert_that(self.descending_first_fit.available_bins).is_length(1)
        assert_that(self.descending_first_fit.most_free_weight_bin.get_free_weight()).is_equal_to(80)

        self.descending_first_fit.put_item_into_new_bin(item=80)

        assert_that(self.descending_first_fit.available_bins).is_length(2)
        # Since the new bin does not have max weight, it should not replace the old one
        assert_that(self.descending_first_fit.most_free_weight_bin.get_free_weight()).is_equal_to(80)

    def test_run_invalid_requests(self) -> None:
        """
        Test that the run method will raise an error with an empty requests list.

        :return:
        """
        with self.assertRaises(ValueError):
            self.descending_first_fit.run(requests=[])

    def test_run_dataset1(self) -> None:
        """
        Test the algorithm produces expected output with dataset 1.

        :return:
        """
        assert_that(self.descending_first_fit.run(requests=self.dataset1)).is_equal_to(5)

    def test_run_dataset2(self) -> None:
        """
        Test the algorithm produces expected output with dataset 2.

        :return:
        """
        assert_that(self.descending_first_fit.run(requests=self.dataset2)).is_equal_to(7)


class TestOrders(unittest.TestCase):
    """Test cases for the Orders class."""

    def setUp(self) -> None:
        """Add an Orders instance to self before each test."""
        self.orders = Orders()

    @patch('src.question2.DescendingFirstFit')
    def test_combine_orders(self, descending_first_fit: Mock) -> None:
        """
        Test that the combine_orders function is working properly.

        :return:
        """
        descending_first_fit.return_value.run.return_value = 4

        assert_that(self.orders.combine_orders(requests=[1, 2, 3], n_max=100)).is_equal_to(4)

        descending_first_fit.return_value.run.assert_called_once_with(requests=[1, 2, 3])
