"""Contain question 1 tests."""

import unittest

from assertpy import assert_that

from src.question1 import Contract, Contracts


class TestContracts(unittest.TestCase):
    """Test cases of the flow of the Contracts class."""

    dataset1 = [
        Contract(1, 1),
        Contract(2, 2),
        Contract(3, 3),
        Contract(4, 4),
        Contract(5, 5),
    ]

    # Different weight and id to ensure the id is being returned, not the weight
    dataset2 = [
        Contract(2, 3),
        Contract(1, 2),
        Contract(4, 5),
        Contract(3, 4),
        Contract(5, 6),
        Contract(9, 10),
        Contract(6, 7),
        Contract(10, 11),
        Contract(8, 9),
        Contract(7, 8),
    ]

    def setUp(self) -> None:
        """Add a Contracts instance to self before each test."""
        self.contracts = Contracts()

    def test_dataset1(self) -> None:
        """
        Test that the get_top_N_open_contracts produces desired output with dataset 1.

        :return:
        """
        top_n = 3

        top_contracts = self.contracts.get_top_N_open_contracts(
            open_contracts=self.dataset1,
            renegotiated_contracts=[],
            top_n=top_n,
        )

        assert_that(top_contracts).is_equal_to([5, 4, 3])

    def test_dataset2(self) -> None:
        """
        Test that the get_top_N_open_contracts produces desired output with dataset 2.

        :return:
        """
        top_n = 5

        top_contracts = self.contracts.get_top_N_open_contracts(
            open_contracts=self.dataset2,
            renegotiated_contracts=[],
            top_n=top_n,
        )

        assert_that(top_contracts).is_equal_to([10, 9, 8, 7, 6])

    def test_renegotiated(self) -> None:
        """
        Test that the get_top_N_open_contracts correctly ignores renegotiated contracts.

        :return:
        """
        top_n = 5

        top_contracts = self.contracts.get_top_N_open_contracts(
            open_contracts=self.dataset2,
            renegotiated_contracts=[10, 8, 7],
            top_n=top_n,
        )

        assert_that(top_contracts).is_equal_to([9, 6, 5, 4, 3])

    def test_top_n_negative(self) -> None:
        """
        Test that the get_top_N_open_contracts returns an empty list if top_n is <= 0.

        :return:
        """
        top_n = -1

        top_contracts = self.contracts.get_top_N_open_contracts(
            open_contracts=self.dataset2,
            renegotiated_contracts=[10, 8, 7],
            top_n=top_n,
        )

        assert_that(top_contracts).is_empty()

    def test_empty_open_contracts(self) -> None:
        """
        Test that the get_top_N_open_contracts return an empty list if the contracts list is empty.

        :return:
        """
        top_n = 5

        top_contracts = self.contracts.get_top_N_open_contracts(
            open_contracts=[],
            renegotiated_contracts=[10, 8, 7],
            top_n=top_n,
        )

        assert_that(top_contracts).is_empty()

    def test_top_n_greater_than_number_of_contracts(self) -> None:
        """
        Test that the get_top_N_open_contracts returns all contracts when top_n is greater than the number of contracts.

        :return:
        """
        top_n = 99

        top_contracts = self.contracts.get_top_N_open_contracts(
            open_contracts=self.dataset2,
            renegotiated_contracts=[10, 8, 7],
            top_n=top_n,
        )

        assert_that(top_contracts).is_equal_to([9, 6, 5, 4, 3, 2, 1])

    def test_all_contracts_renegotiated(self) -> None:
        """
        Test that the get_top_N_open_contracts return an empty list when all contracts have been renegotiated.

        :return:
        """
        top_n = 5

        top_contracts = self.contracts.get_top_N_open_contracts(
            open_contracts=self.dataset1,
            renegotiated_contracts=[5, 4, 3, 2, 1],
            top_n=top_n,
        )

        assert_that(top_contracts).is_empty()
