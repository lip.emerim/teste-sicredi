import random
import time

from src.question1 import Contract, Contracts
from src.question2 import Orders


def run_question_1():
    open_contracts = []

    for i in range(100000):
        contract = Contract(id=i, debt=random.randint(1, 100000))
        open_contracts.append(contract)

    renegotiated = [3]
    top_n = 20

    contracts = Contracts()

    start_time = time.time()

    top_n_contracts = contracts.get_top_N_open_contracts(
        open_contracts=open_contracts,
        renegotiated_contracts=renegotiated,
        top_n=top_n
    )

    print(f'--- {time.time() - start_time} seconds ---')

    print(top_n_contracts)


def run_question_2():
    # orders = [70, 30, 10, 50, 40, 80, 20, 45, 35, 23]

    orders = []

    for i in range(10000):
        orders.append(random.randint(1, 100))

    n_max = 100

    start_time = time.time()

    how_many = Orders().combine_orders(orders, n_max)

    print(f'--- {time.time() - start_time} seconds ---')

    print(how_many)


if __name__ == '__main__':
    run_question_2()
