"""Contain question 1 implementation."""


class Contract:
    """Contract operations."""

    def __init__(self, id, debt):
        """
        Contract constructor.

        :param id: The id of the contract
        :param debt: The debt of the contract
        """
        self.id = id
        self.debt = debt

    def __str__(self):
        """Return a string representation of the contract."""
        return 'id={}, debt={}'.format(self.id, self.debt)  # pragma: no cover # noqa: P101


class Contracts:
    """Contracts operations."""

    def get_top_N_open_contracts(self, open_contracts, renegotiated_contracts, top_n):  # noqa: N802
        """
        Get top_n highest debt non renegotiated contracts from a list of contracts.

        :param open_contracts: The list containing all contracts.
        :param renegotiated_contracts: The list containing ids of renegotiated contracts
        :param top_n: How many contracts to return starting from top
        :return: A list containing the ids of the top contracts
        """
        if top_n <= 0 or not open_contracts:
            return []

        # Filter list to find contracts that were not renegotiated
        # Also create a new list to avoid changing the original one
        debtors_contracts = [contract for contract in open_contracts if contract.id not in renegotiated_contracts]

        # Tested with heapq, and simple algorithms to avoid sorting the whole list,
        # the standard sort still proved to be far superior
        debtors_contracts.sort(key=lambda contract: contract.debt, reverse=True)

        return [contract.id for contract in debtors_contracts][:top_n]
