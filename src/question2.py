"""Contain code for question 2 of the test."""


class Bin:
    """Represent a bin in the bin packing problem."""

    def __init__(self, weight_capacity: float, item_capacity: int):
        """
        Create a new bin.

        :param weight_capacity: The bin weight capacity
        :param item_capacity: The bin item capacity
        """
        self.weight_capacity = weight_capacity
        self.item_capacity = item_capacity
        self.current_weight = 0
        # Could be extracted from len(self.items), but it would cost too much in case of huge capacity bins
        self.item_count = 0
        # Even though the question does not ask for combinations, it makes sense to have the field in the structure
        self.items = []

    def fits(self, weight: float) -> bool:
        """
        Check if it is possible to add a weight to this bin.

        :param weight: Indicate the weight to be added
        :return: Indicate whether or not the bin can fit the weight
        """
        full_weight = self.current_weight + weight > self.weight_capacity
        full_items = self.item_count >= self.item_capacity

        return not (full_items or full_weight)

    def get_free_weight(self) -> float:
        """
        Return the free weight of the bin.

        :return: The free weight of the bin
        """
        return self.weight_capacity - self.current_weight

    def full(self) -> bool:
        """
        Check if the bin can accept more items.

        :return: Indicate if the bin can accept more items
        """
        return self.item_count >= self.item_capacity or self.current_weight >= self.weight_capacity

    def add_item(self, item: str, weight: float, validate: bool = True) -> None:
        """
        Add a new item to the bin.

        :param item: The item label
        :param weight: The item weight
        :param validate: Indicate if we should check before attempting to fit the item
                           In a case a previous check was made, avoid checking twice

        :return: None
        """
        if validate and not self.fits(weight):
            raise ValueError('Item does not fit in bin')

        self.current_weight += weight
        self.item_count += 1
        self.items.append(item)


class DescendingFirstFit:
    """Simple implementation of the descending first fit algorithm."""

    def __init__(self, bin_weight_capacity: float, bin_item_capacity: int):
        """
        Descending first fit constructor.

        :param bin_weight_capacity Indicate the bins max weight
        :param bin_item_capacity Indicate the bins max item count
        """
        if bin_weight_capacity <= 0 or bin_item_capacity <= 0:
            raise ValueError('Capacity parameters must be greater than zero')

        # Remember the bin with most free weight. With that we can avoid looping the
        # list of bins when we know the item cannot fit any.
        self.most_free_weight_bin = None
        self.bin_count = 0
        self.available_bins = []
        self.bin_weight_capacity = bin_weight_capacity
        self.bin_item_capacity = bin_item_capacity

    def initialize(self):
        """Reset state holding variables to neutral."""
        self.bin_count = 0
        self.available_bins = []
        self.most_free_weight_bin = None

    def put_item_into_available_bin(self, item: float) -> bool:
        """
        Given an item, attempt to insert it into one of the available bins.

        :param item: The item to insert into a bin
        :return: A boolean indicating if the item was inserted
        """
        found_bin = None

        for available_bin in self.available_bins:
            # If a bin can fit the item, store the item on it.
            # Usually we would ask for forgiveness
            # by using the validation contained within Bin but, in this case, asking for
            # permission is around 50% faster.
            if available_bin.fits(item):
                found_bin = available_bin
                available_bin.add_item(item=str(item), weight=item, validate=False)
                break

        if found_bin is None:
            return False

        # Since only the bin count matters, full bins can be removed from the list
        if found_bin.full():
            # enumerating for the index and using pop is slower than straight removing the item
            self.available_bins.remove(found_bin)

        # If the bin with most free weight is the current bin, we need to update the
        # most_free_weight_bin variable
        if self.most_free_weight_bin == found_bin:

            if not self.available_bins:
                self.most_free_weight_bin = None
                return True

            self.most_free_weight_bin = max(self.available_bins, key=lambda b: b.get_free_weight())  # noqa: WPS111

        return True

    def put_item_into_new_bin(self, item: float) -> None:
        """
        Given an item, create a new bin and insert the item into it.

        :param item: The item to insert
        :return: None
        """
        new_bin = Bin(self.bin_weight_capacity, self.bin_item_capacity)
        new_bin.add_item(item=str(item), weight=item, validate=False)
        self.available_bins.append(new_bin)
        self.bin_count += 1

        if not self.most_free_weight_bin or self.most_free_weight_bin.get_free_weight() < new_bin.get_free_weight():
            self.most_free_weight_bin = new_bin

    def run(self, requests: list):
        """
        Calculate the minimum amount of bins needed to store a list of items.

        Premisses:
          A bin can contain at most bin_item_capacity items
          A bin can contain at most bin_weight_capacity weight
          Requests is a list of items containing numbers greater than zero and less than bin_weight_capacity

        Use a sorted descending first fit to order requests. This should work great with two item bins
        as the question suggests. Please note that the algorithm is not optimal, it does not find the best
        solution, but attempts to find a good enough solution.

        :param requests Indicate the items to be stored

        :return The minimum amount of bins to store the items
        """
        if not requests:
            raise ValueError('Empty requests list supplied.')

        self.initialize()

        sorted_requests = sorted(requests, reverse=True)

        for request in sorted_requests:
            has_most_free_weight_bin = self.most_free_weight_bin is not None
            most_free_weight = self.most_free_weight_bin.get_free_weight() if has_most_free_weight_bin else 0

            # If any bin can fit the item, start searching
            if request <= most_free_weight:
                self.put_item_into_available_bin(item=request)

                # It is not necessary to continue iteration, the if above ensures that the item fits in
                # at least one bin
                continue

            self.put_item_into_new_bin(item=request)

        return self.bin_count


class Orders:
    """Define orders operations."""

    def combine_orders(self, requests, n_max):
        """
        Calculate minimum trips necessary to transport the requests.

        :param requests: The list of transactions
        :param n_max: The number of transactions per trip
        :return: The minimum number of trips necessary (approximate)
        """
        # In a real application it would be better to pass the algorithm via DI
        # And provide an algorithm interface that exposes the run method
        algorithm = DescendingFirstFit(bin_weight_capacity=n_max, bin_item_capacity=2)

        return algorithm.run(requests=requests)
